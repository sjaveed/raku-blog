use Parameters;

class Request {
    has %!env;
    has Parameters $!params;

    submethod BUILD (:%env) {
        %!env = %env;
        $!params = Parameters.new(:env(%env));
    }

    method add-route-params ( %route-params ) {
        $!params.add(%route-params)
    }

    method param (Str $parameter-name) {
        $!params.one($parameter-name)
    }

    method params {
        $!params.all
    }

    method path-info {
        %!env<PATH_INFO>
    }

    method query-string {
        %!env<QUERY_STRING>
    }
}
