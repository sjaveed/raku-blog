use MONKEY-SEE-NO-EVAL;

enum HttpMethod <delete get options patch post put>;

class Route {
  has HttpMethod $!method;
  has Str $!path;
  has Regex $!regex;
  has Str $!controller-action;

  submethod BUILD ( :$path ) {
    $!path = $path;
  }

  submethod TWEAK ( ) {
    $!regex = self!path-regex;
  }

  method match ( Str $path --> Match ) {
    $path ~~ $!regex
  }

  multi method ACCEPTS ( Str:D: $path --> Match ) {
    self.match($path)
  }

  method to-string {
    "Path: [$!path], Regex: [{ $!regex.raku }]"
  }

  #
  # This method converts the path with embedded path params into a regex that can be used to match against an incoming
  # uri path.  It's called automatically so you should never need to call it manually.
  method !path-regex ( --> Regex ) {
    my @route-path-elements = $!path.split('/');
    my @route-path-regex-elements = [];

    my Str @static-string-matches = [];
    for @route-path-elements -> $route-path-element {
      # Let's add the separator to the end of the current string match
      # since the fact that we're at a word boundary means we encountered one

      if $route-path-element.starts-with(':') {
        # We have a path param...
        @static-string-matches.push('');
        # Let's push the current string match into the regex elements list as a static match
        my $static-string-match = @static-string-matches.join('/');
        @route-path-regex-elements.push("'$static-string-match'");

        # and, since we've already processed the static string match, let's reset it
        @static-string-matches = [''];

        # Now let's figure out the name of this param
        my $param-name = $route-path-element.substr(1);

        # With that, let's craft a regex for this param
        my $param-regex = '$<' ~ $param-name ~ '> = (<+[\S] - [/]>+)';
        @route-path-regex-elements.push($param-regex);
      } else {
        # This isn't a path param
        # let's just add it to the current static string match
        @static-string-matches.push($route-path-element);
      }
    }

    if @static-string-matches.elems > 0 {
      my $static-string-match = @static-string-matches.join('/');
      @route-path-regex-elements.push("'$static-string-match'");
    }

    my $route-path-regex-as-str = "^{ @route-path-regex-elements.join(' ') }\$";
    my $route-path-regex = EVAL "rx \{ $route-path-regex-as-str \}";
  }
}
