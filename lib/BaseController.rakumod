use Request;
use Response;

class BaseController {
  has Request $!request;
  has Response $!response = Response.new;

  submethod BUILD ( Request :$request ) {
    $!request = $request;
  }

  # Crud Actions
  #    method index {}
  #    method new {}
  #    method create {}
  #    method edit {}
  #    method update {}
  #    method destroy {}
}
