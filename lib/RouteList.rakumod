use Route;

class RouteListConfiguration {
  has Route @.routes;

  submethod BUILD {
    @!routes = [];
  }

  method get ( Str $path ) {
    say "Adding route #{ @!routes.elems + 1 }";
    @!routes.push(Route.new(:$path));
  }
}

class RouteList {
  has RouteListConfiguration $!configuration;

  submethod BUILD {
    $!configuration = RouteListConfiguration.new;
  }

  method matching-route ( Str $path --> Route ) {
    my Route $matching-route;

    for $!configuration.routes -> $route {
      my Match $match = $route.match($path);

      next unless $match.Bool;

      # We have a match.  Let's return it
      $matching-route = $route;
      last;
    }

    $matching-route
  }

  multi method ACCEPTS ( Str:D: $path --> Route ) {
    self.matching-route($path)
  }

  method configure ( &route-configurator:( $configuration ) ) {
    route-configurator($!configuration);
  }
}