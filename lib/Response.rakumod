class Response {
    has %!headers;
    has Str $!body;
    has Int $!status-code;

    method render (Str :$text, Int :$status) {
        $!status-code = $status;
        $!body = $text;
    }

    method add-header (Str :$header, Str :$value) {
        %!headers{$header} = $value;
    }

    method for-psgi (--> Array) {
        return [$!status-code, %!headers.Array, [ $!body ]]
    }
}
