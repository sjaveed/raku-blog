use JSON::Tiny;
use Request;
use Response;
use Route;
use RouteList;

#say "Testing to see which route matches...";
#say "/greetings/bob/en-US" ~~ $route-list;

class Application {
  has RouteList $!route-list;

  submethod BUILD {
    $!route-list = RouteList.new;

    # Initialize the route list
    $!route-list.configure: -> $config {
      $config.get("/greetings/:name/:language");
      $config.get("/greetings/:name");
    }
  }

  method handle ( %env ) {
    my $request = Request.new(:%env);
    my $response = Response.new;

    ### Start Routing ###
    my Route $matching-route = self!matching-route($request);

    unless $matching-route {
      # Return a 404 for now
      return self.handle-no-route-matched($request, $response);
    }

    my %route-params = self!route-params($request.path-info, $matching-route);
    $request.add-route-params(%route-params);
    ### End Routing ###

    ### Start Request Processing ###
    my $name = $request.param("name") || "World";
    my $language = $request.param("language") || "en-US";

    $response.add-header(:header( "Content-Type" ), :value( "application/json" ));
    my %json-response = {
      "greeting" => "Hello",
      "target" => $name,
      "language" => $language
    }
    $response.render(:text( to-json %json-response ), :status( 200 ));
    ### End Request Processing ###

    $response.for-psgi
  }

  method handle-no-route-matched ( $request, $response ) {
    $response.render(:text( "Not Found" ), :status( 404 ));

    $response.for-psgi
  }

  method !matching-route ( Request $request --> Route ) {
    my $request-path = $request.path-info;
    say "Attempting to match $request-path against route-list";

    $!route-list.matching-route($request-path)
  }

  method !route-params ( Str $path, Route $route --> Map ) {
    my Match $matches = $route.match($path);
    my %params;

    for $matches.kv -> $k, $v {
      %params{$k} = $v.Str;
    }

    %params;
  }
}