class Parameters {
  has %!env;
  has %!extracted-params;

  submethod BUILD ( :%env ) {
    %!env = %env;
  }

  submethod TWEAK {
    self!extract-params
  }

  method add ( %additional-params ) {
    %!extracted-params.push(%additional-params);
  }

  method one ( Str $parameter-name ) {
    %!extracted-params{$parameter-name}
  }

  method all {
    %!extracted-params
  }

  method !extract-params {
    self!extract-query-params
  }

  method !extract-query-params {
    my @query-parameters = ( %!env<QUERY_STRING> || "" ).split("&");

    for @query-parameters -> $query-parameter {
      my @query-parameter-details = $query-parameter.split("=");
      my $parameter-name = @query-parameter-details[0];
      my $parameter-value = @query-parameter-details[1];

      %!extracted-params{$parameter-name} = $parameter-value;
    }
  }
}