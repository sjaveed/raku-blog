#!/usr/bin/env raku

use lib 'lib';
use Application;

my $app = Application.new;

use HTTP::Easy::PSGI;
my $http = HTTP::Easy::PSGI.new(:port(8080));
$http.handle($app);